# encoding: utf-8

require 'spec_helper'
require 'capybara/rspec'
require 'capybara/poltergeist'

require 'phantomjs' # <-- Not required if your app does Bundler.require automatically (e.g. when using Rails)
Capybara.register_driver :poltergeist do |app|
  Capybara::Poltergeist::Driver.new(app, :phantomjs => Phantomjs.path)
end

Capybara.default_driver    = :poltergeist
Capybara.javascript_driver = :poltergeist

describe "Form filling", :type => :feature do
  before :each do
    visit 'http://sportsmaps.herokuapp.com/'
  end
    
  it "Fill form with data - to search by street name" do
    fill_in 'sports_map[localization]', :with => 'rua da biblioteca'
    fill_in 'sports_map[radius]', :with => '5'
    check('sports_map[options][1]')
    check('sports_map[options][6]')
    uncheck('sports_map[options][6]')
    check('sports_map[options][2]')
    check('sports_map[options][3]')
    check('sports_map[options][4]')
    check('sports_map[options][5]')
    uncheck('sports_map[options][5]')
    click_on('Submit')
  end
  
  it "Fill form with data - to search by code postal" do
    fill_in 'sports_map[localization]', :with => '05508-065'
    click_on('Search')
    fill_in 'sports_map[radius]', :with => '7'
    check('sports_map[options][1]')
    check('sports_map[options][2]')
    check('sports_map[options][3]')
    check('sports_map[options][4]')
    click_on('Submit')
  end
  
  it "Fill form with data - to get the user's location" do
    click_on('btnLocalization')
    fill_in 'sports_map[radius]', :with => '3'
    check('sports_map[options][3]')
    check('sports_map[options][4]')
    click_on('Submit')
    page.driver.resize_window(800, 500)
  end
  
end
