// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .

function menuSize() {
  if ($(window).width()/1.65 > 315){
    return  315;
  } else {
    return $(window).width()/1.65;
  }
}

function menu() {
  if($(".nav").hasClass("side-closed")) {
    $('.nav').animate({
      left: "0px",
    }, 10.0, function() {
      $(".nav").removeClass("side-closed");
    });
     $('.nav-toggle').animate({
      left: (menuSize()),
    }, 100, function() {
    });
  } else {
    $('.nav').animate({
      left: (-menuSize()),
    }, 100, function() {
      $(".nav").addClass("side-closed");
    });
    $('.nav-toggle').animate({
      left: "0px",
    }, 100, function() {
    });
  }
}

//Menu Sidebar
$(window).resize(function() {
  windowSize = $(window).width();
  $(".nav-toggle").remove();
	
  if (windowSize <= 925) { 
    $('.nav').css('left', (-menuSize())).addClass('side-closed');
    $('.menu').append( "<div class='nav-toggle' onClick='menu()'></div>" );
  } else {
    $('.nav').css('left', '0px').addClass('side-closed');
  }  

  menu();
});

$(document).ready(function() {
  windowSize = $(window).width();
  $(".nav-toggle").remove();
  if (windowSize <= 925) { 
    $('.menu').append( "<div class='nav-toggle' onClick='menu()'></div>" );
    if(image==""){
      $('.nav-toggle').css('left',(menuSize()));
    }else{
      $('.nav').css('left', (-menuSize())).addClass('side-closed');
    }
  } else {
    $('.nav').css('left', '0px').addClass('side-closed');
  }

  getLocalization();
});