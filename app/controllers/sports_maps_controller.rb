#require 'open-uri'
require 'net/http'

class SportsMapsController < ApplicationController
  
  @image = "var image = 'default.png'"
  @@areas = ""
  
  def query_params
    params.require(:sports_maps)
  end
  
  
  def index
    # shows map and options and verify if exists a image for draw
    gon.json = @@areas.to_json
    if(session[:image].to_s == '')
      @image =   "''"
    else
      @image=session[:image]
    end
    
    if(session[:localization].to_s == '')
      @localization =   '""'
      @latitude=   '""'
      @longitude=   '""'
      @radius=   '""'
      @op1=   '""'
      @op2=   '""'
      @op3=   '""'
      @op4=   '""'
      @op5=   '""'
      @op6=   '""'
    else
      @localization =   session[:localization]
      @latitude=   session[:latitude]
      @longitude=   session[:longitude]
      @radius=   session[:radius]
      @op1=   session[:op1]
      @op2=   session[:op2]
      @op3=   session[:op3]
      @op4=   session[:op4]
      @op5=   session[:op5]
      @op6=   session[:op6]
    end
    delete_session
  end
  
  def delete_session
    session.delete(:image)
    session.delete(:localization)
    session.delete(:latitude)
    session.delete(:longitude)
    session.delete(:radius)
    session.delete(:op1)
    session.delete(:op2)
    session.delete(:op3)
    session.delete(:op4)
    session.delete(:op5)
    session.delete(:op6)
  end
  
  def loadData
    # captures the parameters , redirects to indexrespond_to do |format| and  captures the name of image for draw in the map
    puts params[:sports_map][:radius]
    respond_to do |format|
      format.html do
        if params[:sports_map][:radius]==""
          flash[:notice] = 'Fill radius field'
        elsif params[:sports_map][:longitude]=="" || params[:sports_map][:radius]==""
          flash[:notice] = 'Click in search'
        elsif !params[:sports_map][:options]
          flash[:notice] = 'Select one checkbox'
          
        else
          #FileUtils.rm_rf(Dir.glob('images/*'))
          if(params[:sports_map][:options]['1'] or params[:sports_map][:options]['2'] or params[:sports_map][:options]['3'] or params[:sports_map][:options]['4'])
            obj = DataProcess.new
            @@areas = obj.loadSensor(params)
            @image =   "'"+obj.processSensors+"'"
          else
            @image="''"
          end
          session[:image]=@image
          session[:localization]="'"+ params[:sports_map][:localization]+"'"
          session[:latitude]="'"+params[:sports_map][:latitude]+"'"
          session[:longitude]="'"+params[:sports_map][:longitude]+"'"
          session[:radius]="'"+params[:sports_map][:radius]+"'"
          session[:op1]="'"+params[:sports_map][:options]['1'].to_s+"'"
          session[:op2]="'"+params[:sports_map][:options]['2'].to_s+"'"
          session[:op3]="'"+params[:sports_map][:options]['3'].to_s+"'"
          session[:op4]="'"+params[:sports_map][:options]['4'].to_s+"'"
          session[:op5]="'"+params[:sports_map][:options]['5'].to_s+"'"
          session[:op6]="'"+params[:sports_map][:options]['6'].to_s+"'"
          
          # coords = Net::HTTP.get_response("api.myjson.com","/bins/50f57");
          # resources = Net::HTTP.get_response("api.myjson.com","/bins/4ie83");
          # parsed = obj.data_to_input_file(coords.body, resources.body)
          
        end
        redirect_to '/'
      end
    end
  end
  
  def deleteFile
    puts params[:image]
    File.delete("./app/assets/images/"+params[:image])
    #@model = Model.create(params[:image])
    #render :json => { :errors => @model.errors.full_messages }, :status => 422
    redirect_to '/'
  end
  
end